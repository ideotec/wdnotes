# WD Notes

WD Notes is a note-taking app that syncs with any WebDAV server.

## Installation

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="75" alt="Get it on F-Droid">](https://f-droid.org/packages/es.ideotec.wdnotes/)

## Usage

The first time you open the app you will have to enter your WebDAV server address and credentials. WD Notes will try to connect to the server and create the "WDNotes" directory where notes will be saved.

To add a note tap on the (+) button on the bottom right corner and then tap on the type of note you want to add. The app supports 5 different types of note:
- **Text**: a markdown-formatted text
- **Checklist**:  a list of items that can be checked off
- **Link**: a URL that can be shared directly from the browser
- **Image**: an image (NOT IMPLEMENTED YET)
- **Audio**: a recorded audio message (NOT IMPLEMENTED YET)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)