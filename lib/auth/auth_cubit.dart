import 'package:hydrated_bloc/hydrated_bloc.dart';

import 'auth_model.dart';
import 'auth_repository.dart';
import 'auth_state.dart';

class AuthCubit extends HydratedCubit<AuthState> {
  final AuthRepository repository;

  AuthCubit(this.repository) : super(AuthInitial());

  test(Auth auth) async {
    emit(AuthTesting(auth));
    try {
      await repository.testAuth(auth);
      emit(AuthComplete(auth));
    } on Exception {
      emit(AuthError(auth,
          'Couldn\'t connect to ${auth.url}.\nPlease check that the URL is correct and that you are connected to the Internet.'));
    } catch (statusCode) {
      String message;
      if (statusCode == 404) {
        message =
            'Couldn\'t find a WebDAV instance at that address.\nPlease check that the URL is correct.';
      } else if (statusCode == 401) {
        message =
            'Authentication failed with these credentials. Please check that the username and password are correct.';
      }
      emit(AuthError(auth, message));
    }
  }

  @override
  AuthState fromJson(Map<String, dynamic> json) {
    var auth = Auth(
        url: Uri.parse(json['url']),
        username: json['username'],
        password: json['password']);
    return AuthComplete(auth);
  }

  @override
  Map<String, dynamic> toJson(AuthState state) {
    if (state is AuthComplete) {
      return state.auth.toJson();
    } else {
      return null;
    }
  }
}
