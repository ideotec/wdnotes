import 'package:equatable/equatable.dart';

class Auth extends Equatable {
  final Uri url;
  final String username;
  final String password;

  const Auth({this.url, this.username, this.password});

  factory Auth.fromJson(Map<String, dynamic> json) => Auth(
      url: Uri.parse(json['url']),
      username: json['username'] as String,
      password: json['password'] as String);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'url': url.toString(),
        'username': username,
        'password': password
      };

  @override
  List<Object> get props => [url, username, password];

  @override
  bool get stringify => true;
}
