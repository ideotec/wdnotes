import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'auth_cubit.dart';
import 'auth_model.dart';
import 'auth_state.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({Key key}) : super(key: key);

  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final _formKey = GlobalKey<FormState>();
  Uri _url;
  String _username;
  String _password;

  void _submit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      FocusScope.of(context).requestFocus(FocusNode());
      BlocProvider.of<AuthCubit>(context)
          .test(Auth(url: _url, username: _username, password: _password));
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context).authTitle)),
      body: BlocConsumer<AuthCubit, AuthState>(listener: (context, state) {
        if (state is AuthError) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(state.message, textAlign: TextAlign.center)));
        }
      }, builder: (context, state) {
        return LoadingOverlay(
            isLoading: (state is AuthTesting),
            child: Form(
                key: _formKey,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextFormField(
                          decoration: InputDecoration(
                              icon: const Icon(Icons.home),
                              labelText: AppLocalizations.of(context).urlLabel,
                              hintText:
                                  AppLocalizations.of(context).urlHintText),
                          validator: (value) {
                            return (value.isEmpty)
                                ? AppLocalizations.of(context).required
                                : null;
                          },
                          onSaved: (value) => _url = Uri.parse(value.trim())),
                      TextFormField(
                          decoration: InputDecoration(
                              icon: const Icon(Icons.person),
                              labelText:
                                  AppLocalizations.of(context).usernameLabel,
                              hintText: AppLocalizations.of(context)
                                  .usernameHintText),
                          validator: (value) {
                            return (value.isEmpty)
                                ? AppLocalizations.of(context).required
                                : null;
                          },
                          onSaved: (value) => _username = value.trim()),
                      TextFormField(
                          decoration: InputDecoration(
                              icon: const Icon(Icons.home),
                              labelText:
                                  AppLocalizations.of(context).passwordLabel,
                              hintText: AppLocalizations.of(context)
                                  .passwordHintText),
                          obscureText: true,
                          validator: (value) {
                            return (value.isEmpty)
                                ? AppLocalizations.of(context).required
                                : null;
                          },
                          onSaved: (value) => _password = value.trim()),
                      ElevatedButton(
                          child: Text(AppLocalizations.of(context).save),
                          onPressed: _submit)
                    ])));
      }));
}
