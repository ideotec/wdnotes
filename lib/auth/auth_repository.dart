import 'package:http_auth/http_auth.dart';

import 'auth_model.dart';

class AuthRepository {
  Future<void> testAuth(Auth auth) async {
    final response =
        await BasicAuthClient(auth.username, auth.password).head(auth.url);
    if (response.statusCode != 200) throw response.statusCode;
  }
}
