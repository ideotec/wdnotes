import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'auth/auth_cubit.dart';
import 'auth/auth_page.dart';
import 'auth/auth_repository.dart';
import 'auth/auth_state.dart';
import 'notes/pages/notes_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  HydratedBlocOverrides.runZoned(
    () => runApp(const WDNotes()),
    storage: storage,
  );
}

class WDNotes extends StatelessWidget {
  const WDNotes({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'WD Notes',
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        theme: ThemeData(
            primarySwatch: Colors.orange,
            snackBarTheme: SnackBarThemeData(
                backgroundColor: Colors.red[900],
                contentTextStyle: const TextStyle(fontSize: 16))),
        home: BlocProvider<AuthCubit>(
            create: (context) => AuthCubit(AuthRepository()),
            child: BlocBuilder<AuthCubit, AuthState>(builder: (context, state) {
              if (state is AuthComplete) return NotesPage(auth: state.auth);
              return const AuthPage();
            })));
  }
}
