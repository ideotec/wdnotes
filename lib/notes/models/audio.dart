import 'dart:typed_data';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'extended_note.dart';

class Audio extends Equatable implements ExtendedNote {
  final Uint8List audio;

  const Audio({@required this.audio});

  factory Audio.fromJson(Map<String, dynamic> json) =>
      Audio(audio: Uint8List.fromList(json['audio'].codeUnits));

  @override
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'type': 'audio', 'audio': String.fromCharCodes(audio)};

  @override
  Audio copyWith({@required Uint8List audio}) =>
      Audio(audio: audio ?? this.audio);

  @override
  bool isEmpty() => audio.isEmpty;

  @override
  List<Object> get props => [audio];
}
