import 'dart:io';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:xml/xml.dart';

import 'extended_note.dart';
import 'note.dart';

class BaseNote extends Equatable {
  final String filename;
  final int modified;
  final String etag;
  final String contenttype;

  const BaseNote(
      {@required this.filename,
      @required this.modified,
      @required this.etag,
      @required this.contenttype});

  factory BaseNote.fromXml(XmlElement xml) {
    final prop = xml
        .getElement('propstat', namespace: '*')
        .getElement('prop', namespace: '*');
    return BaseNote(
        filename: basename(xml.getElement('href', namespace: '*').text),
        modified: HttpDate.parse(
                prop.getElement('getlastmodified', namespace: '*').text)
            .millisecondsSinceEpoch,
        etag: prop.getElement('getetag', namespace: '*').text,
        contenttype: prop.getElement('getcontenttype', namespace: '*').text);
  }

  factory BaseNote.fromJson(Map<String, dynamic> json) => BaseNote(
      filename: json['filename'] as String,
      modified: json['modified'] as int,
      etag: json['etag'] as String,
      contenttype: json['contenttype'] as String);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'filename': filename,
        'modified': modified,
        'etag': etag,
        'contenttype': contenttype
      };

  Note toNote({@required ExtendedNote extendedNote}) => Note(
      filename: filename,
      modified: modified,
      etag: etag,
      contenttype: contenttype,
      extendedNote: extendedNote);

  @override
  List<Object> get props => [filename, modified, etag, contenttype];

  @override
  bool get stringify => true;
}
