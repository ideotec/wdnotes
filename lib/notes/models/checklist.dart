import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'extended_note.dart';

class Checklist extends Equatable implements ExtendedNote {
  final Map<String, bool> checklist;

  const Checklist({@required this.checklist});

  factory Checklist.fromJson(Map<String, dynamic> json) => Checklist(
      checklist: (json['checklist'] as Map<String, dynamic>)
          ?.map((k, e) => MapEntry(k, e as bool)));

  @override
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'type': 'checklist', 'checklist': checklist};

  @override
  Checklist copyWith({Map<String, bool> checklist}) =>
      Checklist(checklist: checklist ?? Map.of(this.checklist));

  @override
  bool isEmpty() => checklist.isEmpty;

  @override
  List<Object> get props => [checklist];
}
