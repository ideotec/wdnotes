import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'extended_note.dart';

class Document extends Equatable implements ExtendedNote {
  final String text;

  const Document({@required this.text});

  factory Document.fromJson(Map<String, dynamic> json) =>
      Document(text: json['text'] as String);

  @override
  Map<String, dynamic> toJson() =>
      <String, dynamic>{'type': 'document', 'text': text};

  @override
  Document copyWith({String text}) => Document(text: text ?? this.text);

  @override
  bool isEmpty() => text.isEmpty;

  @override
  List<Object> get props => [text];
}
