import 'audio.dart';
import 'checklist.dart';
import 'document.dart';
import 'link.dart';
import 'picture.dart';

abstract class ExtendedNote {
  factory ExtendedNote.fromJson(Map<String, dynamic> json) {
    switch (json['type']) {
      case 'document':
        return Document.fromJson(json);
      case 'checklist':
        return Checklist.fromJson(json);
      case 'link':
        return Link.fromJson(json);
      case 'picture':
        return Picture.fromJson(json);
      case 'audio':
        return Audio.fromJson(json);
      default:
        return null;
    }
  }

  Map<String, dynamic> toJson();

  ExtendedNote copyWith();

  bool isEmpty();
}
