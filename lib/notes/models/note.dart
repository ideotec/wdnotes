import 'package:flutter/foundation.dart';

import 'base_note.dart';
import 'extended_note.dart';

class Note extends BaseNote {
  final ExtendedNote extendedNote;

  const Note(
      {@required String filename,
      @required int modified,
      @required String etag,
      @required String contenttype,
      @required this.extendedNote})
      : super(
            filename: filename,
            modified: modified,
            etag: etag,
            contenttype: contenttype);

  factory Note.fromJson(Map<String, dynamic> json) => Note(
      filename: json['filename'] as String,
      modified: json['modified'] as int,
      etag: json['etag'] as String,
      contenttype: json['contenttype'] as String,
      extendedNote: json['extendedNote'] == null
          ? null
          : ExtendedNote.fromJson(
              json['extendedNote'] as Map<String, dynamic>));

  factory Note.fromExtendedNote(ExtendedNote extendedNote) => Note(
      filename: '',
      modified: 0,
      etag: '',
      contenttype: '',
      extendedNote: extendedNote);

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{
        'filename': filename,
        'modified': modified,
        'etag': etag,
        'contenttype': contenttype,
        'extendedNote': extendedNote.toJson()
      };

  Note copyWith(
          {String filename,
          String modified,
          String etag,
          String contenttype,
          ExtendedNote extendedNote}) =>
      Note(
          filename: filename ?? this.filename,
          modified: modified ?? this.modified,
          etag: etag ?? this.etag,
          contenttype: contenttype ?? this.contenttype,
          extendedNote: extendedNote ?? this.extendedNote);

  BaseNote toBaseNote() => BaseNote(
      filename: filename,
      modified: modified,
      etag: etag,
      contenttype: contenttype);

  @override
  List<Object> get props =>
      [filename, modified, etag, contenttype, extendedNote];
}
