import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import 'extended_note.dart';

class Picture extends Equatable implements ExtendedNote {
  final Uint8List image;

  const Picture({@required this.image});

  factory Picture.fromJson(Map<String, dynamic> json) =>
      Picture(image: Uint8List.fromList(json['image'].codeUnits));

  @override
  Map<String, dynamic> toJson() => <String, dynamic>{
        'type': 'picture',
        'image': String.fromCharCodes(image)
      };

  @override
  Picture copyWith({Uint8List image}) => Picture(image: image ?? this.image);

  @override
  bool isEmpty() => image.isEmpty;

  @override
  List<Object> get props => [image];
}
